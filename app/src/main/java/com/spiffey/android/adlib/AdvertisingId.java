package com.spiffey.android.adlib;

import android.content.Context;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import java.io.IOException;

public final class AdvertisingId {

    private AdvertisingIdClient.Info mAdvertisingIdClient;

    private static AdvertisingId sAdvertisingId;

    private AdvertisingId(final Context context) throws IOException,
            GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
            Thread t = new Thread() {
                @Override
                public void run() {
                    super.run();
                    try {
                        mAdvertisingIdClient = AdvertisingIdClient.getAdvertisingIdInfo(context);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    }
                }
            };

    }

    public static AdvertisingId getInstance(Context context) throws IOException,
            GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        if (sAdvertisingId == null) {
            sAdvertisingId = new AdvertisingId(context);
        }
        return sAdvertisingId;
    }

    public String getId() {
        return mAdvertisingIdClient.getId();
    }

    public boolean isLimitAdTrackingEnabled() {
        return mAdvertisingIdClient.isLimitAdTrackingEnabled();
    }
}
